
  
# CurrencyApiClient [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) ![Nuget](https://img.shields.io/nuget/v/Skelp.CurrencyApiClient)

  

Get hourly updated information on currencies using this unofficial client to access [CURRENCY API](https://currency.getgeoapi.com/) by API PLANT.
  

## Table of Contents

2. [Installation](#installation)

4. [Getting Started](#getting-started)

  

## Installation

  

Simply use the Visual Studio NuGet Package Manager and look for `CurrencyApiClient`.

  

Alternatively, you can use the Package Manager Console as such:

>  `Install-Package Skelp.CurrencyApiClient`

  

If you want to use the .NET CLI, you can use the following command:

>  `dotnet add package Skelp.CurrencyApiClient`


## Getting Started


```csharp
using Skelp.ApiPlant.CurrencyApi.Client;

```

`[...]`

```csharp
string myApiKey = File.ReadAllText("apikey.txt");
CurrencyApiClient client = new CurrencyApiClient(myApiKey);

// Get all currencies with their respective ISO code and name
CurrencyCollection currencies = await client.ListAsync();

// Get hourly accurate data for converting 20 US Dollar to Euro
CurrencyConversion conversion = await client.ConvertAsync(from: "USD", to: "EUR", amount: 20);
```

  
