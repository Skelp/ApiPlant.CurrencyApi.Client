using NUnit.Framework;
using Skelp.ApiPlant.CurrencyApi.Client.Interfaces;
using Skelp.ApiPlant.CurrencyApi.Client.Models;
using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace Skelp.ApiPlant.CurrencyApi.Client.Tests
{
    [Explicit]
    public class CurrencyApiClientIntegrationTests
    {
        private string apiKey;

        private IAsyncCurrencyApiClient currencyApiClient;

        [Test]
        public async Task ConvertAsync()
        {
            CurrencyConversion result = default;
            Assert.DoesNotThrowAsync(async () => result = await currencyApiClient.ConvertAsync(to: "USD"));
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Rates, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public async Task HistoricalAsync()
        {
            CurrencyConversion result = default;
            Assert.DoesNotThrowAsync(async () => result = await currencyApiClient.HistoricalAsync(date: DateTime.Now - TimeSpan.FromDays(14), to: "USD"));
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Rates, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public async Task ListAsync()
        {
            CurrencyCollection result = default;
            Assert.DoesNotThrowAsync(async () => result = await currencyApiClient.ListAsync());
            Assert.That(result, Is.Not.Null.And.Not.Empty);
        }

        [OneTimeSetUp]
        public async Task OneTimeSetup()
        {
            await EnsureHostAvailabilityAsync(CurrencyApiClient.ApiBase.Host);
            apiKey = File.ReadAllText(@".\apikey");
            currencyApiClient = new CurrencyApiClient(apiKey);
        }

        [SetUp]
        public void Setup()
        {
        }

        private async Task EnsureHostAvailabilityAsync(string host)
        {
            var ping = new Ping();
            var result = await ping.SendPingAsync(host);

            if (result.Status != IPStatus.Success)
            {
                throw new NetworkInformationException();
            }
        }
    }
}
