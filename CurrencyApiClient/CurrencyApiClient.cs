﻿using Skelp.ApiPlant.CurrencyApi.Client.Extensions;
using Skelp.ApiPlant.CurrencyApi.Client.Interfaces;
using Skelp.ApiPlant.CurrencyApi.Client.Mappers;
using Skelp.ApiPlant.CurrencyApi.Client.Models;
using Skelp.WebApi.Base;
using Skelp.WebApi.Base.Interfaces;
using Skelp.WebApi.Base.Mappers;
using Skelp.WebApi.Base.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Skelp.ApiPlant.CurrencyApi.Client
{
    /// <summary>
    /// Provides methods for accessing the CURRENCY API by API PLANT.
    /// </summary>
    public class CurrencyApiClient : IAsyncCurrencyApiClient
    {
        public static Uri ApiBase = new Uri(@"https://api.getgeoapi.com/v2/currency");

        private const string API_KEY_PARAM = "api_key";

        private const string FORMAT_PARAM = "format";

        private const string USED_FORMAT = "json";

        private readonly string apiKey;

        private readonly IJsonMapper<CurrencyCollection> currencyCollectionMapper;

        private readonly IJsonMapper<CurrencyConversion> currencyConversionMapper;

        private readonly IDataClient dataClient;

        private readonly IJsonMapper<ResponseFailure> responseFailureMapper;

        /// <summary>
        /// Default constructor for use without IoC container, with default implementation.
        /// </summary>
        /// <param name="apiKey">Any valid api key.</param>
        /// <exception cref="ArgumentException">
        /// Thrown if <paramref name="apiKey"/> is null or empty.
        /// </exception>
        public CurrencyApiClient(string apiKey)
        {
            if (string.IsNullOrEmpty(apiKey))
            {
                throw new ArgumentException($"'{nameof(apiKey)}' cannot be null or empty.", nameof(apiKey));
            }

            var userAgent = new UserAgent();
            var config = new DataClientConfig();
            dataClient = new DataClient(userAgent, config);
            currencyCollectionMapper = new CurrencyCollectionMapper();
            currencyConversionMapper = new CurrencyConversionMapper();
            responseFailureMapper = new ResponseFailureMapper();
            this.apiKey = apiKey;
        }

        /// <summary>
        /// Constructor used for dependency injection with an IoC container.
        /// </summary>
        /// <param name="dataClient">Any valid <see cref="IDataClient"/>.</param>
        /// <param name="currencyCollectionMapper">Any valid <see cref="IJsonMapper{CurrencyCollection}"/>.</param>
        /// <param name="currencyConversionMapper">Any valid <see cref="IJsonMapper{CurrencyConversion}"/>.</param>
        /// <param name="responseFailureMapper">Any valid <see cref="IJsonMapper{ResponseFailure}"/>.</param>
        /// <param name="apiKey">Any valid api key.</param>
        /// <exception cref="ArgumentException">
        /// Thrown if <paramref name="apiKey"/> is null or empty.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// Thrown if any parameter, other than <paramref name="apiKey"/>, is null.
        /// </exception>
        public CurrencyApiClient(IDataClient dataClient, IJsonMapper<CurrencyCollection> currencyCollectionMapper,
            IJsonMapper<CurrencyConversion> currencyConversionMapper, IJsonMapper<ResponseFailure> responseFailureMapper, string apiKey)
        {
            if (string.IsNullOrEmpty(apiKey))
            {
                throw new ArgumentException($"'{nameof(apiKey)}' cannot be null or empty.", nameof(apiKey));
            }

            this.dataClient = dataClient ?? throw new ArgumentNullException(nameof(dataClient));
            this.currencyCollectionMapper = currencyCollectionMapper ?? throw new ArgumentNullException(nameof(currencyCollectionMapper));
            this.currencyConversionMapper = currencyConversionMapper ?? throw new ArgumentNullException(nameof(currencyConversionMapper));
            this.responseFailureMapper = responseFailureMapper ?? throw new ArgumentNullException(nameof(responseFailureMapper));
            this.apiKey = apiKey;
        }

        /// <inheritdoc/>
        public CurrencyConversion Convert(string from = "EUR", string to = null, decimal amount = 1) => ConvertAsync(from, to, amount).GetAwaiter().GetResult();

        /// <inheritdoc/>
        public async Task<CurrencyConversion> ConvertAsync(string from = "EUR", string to = null, decimal amount = 1, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(from))
            {
                throw new ArgumentException($"'{nameof(from)}' cannot be null or whitespace.", nameof(from));
            }

            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"'{nameof(amount)}' cannot be 0 or less.");
            }

            var request = CreateBaseRequestUri("convert")
                .AddQuery(nameof(from), from)
                .AddQuery(nameof(amount), amount.ToString());

            if (string.IsNullOrWhiteSpace(to) == false)
            {
                request = request.AddQuery(nameof(to), to);
            }

            var jsonResponse = await GetResponseAsync(request, cancellationToken);
            return currencyConversionMapper.Map(jsonResponse.ResponseObject);
        }

        /// <inheritdoc/>
        public CurrencyConversion Historical(DateTime date, string from = "EUR", string to = null, decimal amount = 1) => HistoricalAsync(date, from, to, amount).GetAwaiter().GetResult();

        /// <inheritdoc/>
        public async Task<CurrencyConversion> HistoricalAsync(DateTime date, string from = "EUR", string to = null, decimal amount = 1, CancellationToken cancellationToken = default)
        {
            if (date > DateTime.UtcNow.Date)
            {
                throw new ArgumentOutOfRangeException(nameof(date), date, $"'{nameof(date)}' cannot be in the future.");
            }

            if (string.IsNullOrWhiteSpace(from))
            {
                throw new ArgumentException($"'{nameof(from)}' cannot be null or whitespace.", nameof(from));
            }

            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"'{nameof(amount)}' cannot be 0 or less.");
            }

            var request = CreateBaseRequestUri($"historical/{date.ToString("yyyy-MM-dd")}")
                .AddQuery(nameof(from), from)
                .AddQuery(nameof(amount), amount.ToString());

            if (string.IsNullOrWhiteSpace(to) == false)
            {
                request = request.AddQuery(nameof(to), to);
            }

            var jsonResponse = await GetResponseAsync(request, cancellationToken);
            return currencyConversionMapper.Map(jsonResponse.ResponseObject);
        }

        /// <inheritdoc/>
        public CurrencyCollection List() => ListAsync().GetAwaiter().GetResult();

        /// <inheritdoc/>
        public async Task<CurrencyCollection> ListAsync(CancellationToken cancellationToken = default)
        {
            var request = CreateBaseRequestUri("list");
            var jsonResponse = await GetResponseAsync(request, cancellationToken);
            return currencyCollectionMapper.Map(jsonResponse.ResponseObject);
        }

        private Uri CreateBaseRequestUri(string endpoint)
        {
            return new Uri(ApiBase.ToString())
                .Append(endpoint)
                .AddQuery(API_KEY_PARAM, apiKey)
                .AddQuery(FORMAT_PARAM, USED_FORMAT);
        }

        private async Task<Response<string>> GetResponseAsync(Uri request, CancellationToken cancellationToken = default)
        {
            var jsonResponse = await dataClient.GetAsync(request, cancellationToken);
            ThrowOnFailure(jsonResponse);
            return jsonResponse;
        }

        private void ThrowOnFailure(Response<string> response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            var responseFailure = responseFailureMapper.Map(response.ResponseObject);
            throw new ResponseFailureException(responseFailure);
        }
    }
}
