﻿using Skelp.ApiPlant.CurrencyApi.Client.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Skelp.ApiPlant.CurrencyApi.Client.Interfaces
{
    public interface IAsyncCurrencyApiClient : ICurrencyApiClient
    {
        /// <summary>
        /// Currency Conversion Lookup converts one currency to another.
        /// </summary>
        /// <param name="from">The base currency.
        /// <para>The default base currency is EUR.</para></param>
        /// <param name="to">The currency to convert to.
        /// <para>If the value is not provided then a list of all supported currencies will be returned.</para></param>
        /// <param name="amount">The amount that needs to be converted.
        /// <para>The default value is 1.</para></param>
        /// <returns></returns>
        Task<CurrencyConversion> ConvertAsync(string from = "EUR", string to = default, decimal amount = 1, CancellationToken cancellationToken = default);

        /// <summary>
        /// Currency Conversion Lookup converts one currency to another.
        /// </summary>
        /// <param name="date">The date for which to request rates. </param>
        /// <param name="from">The base currency.
        /// <para>The default base currency is EUR.</para></param>
        /// <param name="to">The currency to convert to.
        /// <para>If the value is not provided then a list of all supported currencies will be returned.</para></param>
        /// <param name="amount">The amount that needs to be converted.
        /// <para>The default value is 1.</para></param>
        /// <returns></returns>
        Task<CurrencyConversion> HistoricalAsync(DateTime date, string from = "EUR", string to = default, decimal amount = 1, CancellationToken cancellationToken = default);

        /// <summary>
        /// Currencies List Lookup returns the list of all supported currencies.
        /// </summary>
        /// <returns></returns>
        Task<CurrencyCollection> ListAsync(CancellationToken cancellationToken = default);
    }
}
