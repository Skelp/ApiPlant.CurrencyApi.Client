﻿using Skelp.WebApi.Base.Models;
using System;

namespace Skelp.ApiPlant.CurrencyApi.Client
{
    internal class DataClientConfig : DataClientBaseConfig
    {
        public override string AssemblyName => typeof(DataClientConfig).Assembly.GetName().Name;

        public override string AssemblyVersion => typeof(DataClientConfig).Assembly.GetName().Version.ToString();

        public override string ProjectFriendlyName => "CURRENCY API Client";

        public override Uri ProjectUrl => new Uri(@"https://gitlab.com/Skelp/ApiPlant.CurrencyApi.Client");
    }
}
