﻿using Newtonsoft.Json;
using Skelp.ApiPlant.CurrencyApi.Client.Models;
using Skelp.WebApi.Base.Interfaces;

namespace Skelp.ApiPlant.CurrencyApi.Client.Mappers
{
    public class CurrencyConversionMapper : IJsonMapper<CurrencyConversion>
    {
        public CurrencyConversion Map(string json)
        {
            dynamic o = JsonConvert.DeserializeObject(json);
            var result = new CurrencyConversion()
            {
                Amount = o.amount,
                BaseCurrency = new Currency()
                { Code = o.base_currency_code, Name = o.base_currency_name },
                UpdatedDate = o.updated_date,
                Rates = new ConversionRateCollection()
            };

            foreach (var rate in o.rates)
            {
                var newRate = new ConversionRate()
                {
                    CurrencyName = rate.Value.currency_name,
                    Rate = rate.Value.rate,
                    RateForAmount = rate.Value.rate_for_amount
                };

                result.Rates.Add(rate.Name, newRate);
            }

            return result;
        }
    }
}
