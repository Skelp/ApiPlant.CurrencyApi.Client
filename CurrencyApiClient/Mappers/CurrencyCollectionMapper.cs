﻿using Newtonsoft.Json;
using Skelp.ApiPlant.CurrencyApi.Client.Models;
using Skelp.WebApi.Base.Interfaces;

namespace Skelp.ApiPlant.CurrencyApi.Client.Mappers
{
    public class CurrencyCollectionMapper : IJsonMapper<CurrencyCollection>
    {
        public CurrencyCollection Map(string json)
        {
            dynamic o = JsonConvert.DeserializeObject(json);
            var result = new CurrencyCollection();

            foreach (var currency in o.currencies)
            {
                result.Add(new Currency() { Code = currency.Name, Name = currency.Value });
            }

            return result;
        }
    }
}
