﻿namespace Skelp.ApiPlant.CurrencyApi.Client.Models
{
    public class ConversionRate
    {
        /// <summary>
        /// Name of the required currency.
        /// </summary>
        public string CurrencyName { get; set; }

        /// <summary>
        /// The conversion rate value for amount = 1.
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// The conversion rate value for requested amount.
        /// </summary>
        public decimal RateForAmount { get; set; }
    }
}
