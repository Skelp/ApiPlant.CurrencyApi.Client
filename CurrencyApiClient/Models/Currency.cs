﻿namespace Skelp.ApiPlant.CurrencyApi.Client.Models
{
    public class Currency
    {
        /// <summary>
        /// The ISO code of the currency.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The english, readable name of the currency.
        /// </summary>
        public string Name { get; set; }
    }
}
