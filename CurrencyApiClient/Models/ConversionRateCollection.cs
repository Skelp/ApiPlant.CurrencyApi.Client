﻿using System.Collections.Generic;

namespace Skelp.ApiPlant.CurrencyApi.Client.Models
{
    public class ConversionRateCollection : Dictionary<string, ConversionRate>
    {
    }
}
