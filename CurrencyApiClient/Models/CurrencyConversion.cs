﻿using System;

namespace Skelp.ApiPlant.CurrencyApi.Client.Models
{
    public class CurrencyConversion
    {
        /// <summary>
        /// The amount that needs to be converted.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The base currency.
        /// </summary>
        public Currency BaseCurrency { get; set; }

        /// <summary>
        /// The exchange rates values.
        /// </summary>
        public ConversionRateCollection Rates { get; set; }

        /// <summary>
        /// The date when the currency rate was updated.
        /// </summary>
        public DateTime UpdatedDate { get; set; }
    }
}
